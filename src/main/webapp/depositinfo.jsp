<%@ page language="java" isELIgnored="false" contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <meta charset="UTF-8">
    <title>Вклады</title>
</head>
    <body>
        <h1>Список вкладов:</h1>

        <c:forEach items="${depositList}" var="deposit">
            ID_Владельца: ${deposit.persID} <br>
            Начальная сумма: ${deposit.startSum} <br>
            Начальная дата: ${deposit.startDate} <br>
            Процент: ${deposit.percent} <hr>
        </c:forEach>

    </body>
</html>