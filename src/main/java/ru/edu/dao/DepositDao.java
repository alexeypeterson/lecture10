package ru.edu.dao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.edu.JDBC.Deposit;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("DuplicatedCode")
@Service
public class DepositDao {

    @Value("${connection.db.string}")
    private String localUrl;


    /**
     * Создаем запись о новом вкладе в БД
     */
    public boolean createDeposit(Deposit deposit){
        boolean result = false;
        final String createDepositPrepStatement = "insert into Deposit(Pers_ID, Start_sum, Start_date, Persent) values(?,?,?,?)";

        try (Connection connection = DriverManager.getConnection(localUrl);
             PreparedStatement preparedStatement = connection.prepareStatement(createDepositPrepStatement)){
                preparedStatement.setInt(1, deposit.getPersID());
                preparedStatement.setInt(2, deposit.getStartSum());
                preparedStatement.setDate(3, (Date) deposit.getStartDate());
                preparedStatement.setDouble(4, deposit.getPercent());
                preparedStatement.executeUpdate();
                result = true;

        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return result;
    }

    /**
     * Получаем список всех вкладов
     */
    public List<Deposit> readAllDeposits(){
        final String readAllDepositsPrepStatement = "select * from Deposit";

        try (Connection connection = DriverManager.getConnection(localUrl);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(readAllDepositsPrepStatement)){
            List<Deposit> depositList = new ArrayList<>();

            while (resultSet.next()){
                Deposit tempDeposit = new Deposit(
                        resultSet.getInt("Pers_ID"),
                        resultSet.getInt("Start_sum"),
                        resultSet.getDate("Start_date"),
                        resultSet.getInt("Percent"));
                depositList.add(tempDeposit);
            }
            return depositList;
        }catch (SQLException ex){
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Получаем вклад по его ID
     */
    public List<Deposit> readOneDeposits(int depositId){
        final String readAllDepositsPrepStatement = "select * from Deposit where Dep_ID = ?";

        try (Connection connection = DriverManager.getConnection(localUrl);
             PreparedStatement statement = connection.prepareStatement(readAllDepositsPrepStatement);
             ){
            List<Deposit> depositList = new ArrayList<>();
            statement.setInt(1, depositId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Deposit tempDeposit = new Deposit(
                        resultSet.getInt("Pers_ID"),
                        resultSet.getInt("Start_sum"),
                        resultSet.getDate("Start_date"),
                        resultSet.getInt("Percent"));
                depositList.add(tempDeposit);
            }
            return depositList;
        }catch (SQLException ex){
            ex.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Удаление вклада по ID
     */
    public boolean deleteDepositById(int id){
        final String deleteDepositPreparedStatement = "delete from Deposit where Dep_ID = ?";
        boolean result = false;
        try(Connection connection = DriverManager.getConnection(localUrl);
        PreparedStatement preparedStatement = connection.prepareStatement(deleteDepositPreparedStatement))
        {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            result = true;
        }catch (SQLException ex){
        ex.printStackTrace();
    }
        return result;
    }

}



