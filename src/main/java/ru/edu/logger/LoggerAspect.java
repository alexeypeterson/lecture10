package ru.edu.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Aspect
@Component
public class LoggerAspect {

    @Pointcut("execution(* ru.edu.controller.DepositViewController.*(..))")
    public void depositViewPointcut() {
    }


    /**
     * Запуск логирования перед выполнением метода
     */
    @Before("depositViewPointcut()")
    public void beforeMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = Arrays.stream(joinPoint.getArgs())
                .map(e -> "" + e)
                .collect(Collectors.toList());
        System.out.println("Method = " + methodName + " args = " + args + " started");
    }


    /**
     * Запуск логирования после выполнением метода
     */
    @AfterReturning("depositViewPointcut()")
    public void afterMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = Arrays.stream(joinPoint.getArgs())
                .map(e -> "" + e)
                .collect(Collectors.toList());
        System.out.println("Method = " + methodName + " args = " + args + " ended");
    }

}
