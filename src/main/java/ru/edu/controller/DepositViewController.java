package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.JDBC.Deposit;
import ru.edu.dao.DepositDao;

import java.util.List;

@RestController
public class DepositViewController {

    private final DepositDao dao;

    @Autowired
    public DepositViewController(DepositDao dao) {
        this.dao = dao;
    }

    /**
     * Отображение приветственной страницы
     */
    @GetMapping(value = "/main")
    public ModelAndView main() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/main.jsp");
        return modelAndView;
    }

    /**
     * Отображение информации по всем существующим вкладам
     */
    @GetMapping(value = "/depositinfo")
    public ModelAndView getAllDeposits(Model model) {
        List<Deposit> depositList = dao.readAllDeposits();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("depositinfo.jsp");
        modelAndView.addObject("depositList", depositList);
        return modelAndView;
    }

    /**
     * Отображение информации о вкладе по его ID
     */
    @GetMapping(value = "/depositinfo/{id}")
    public ModelAndView getOneDeposits(Model model, @PathVariable int id) {
        List<Deposit> depositList = dao.readOneDeposits(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("depositinfo.jsp");
        modelAndView.addObject("depositList", depositList);
        return modelAndView;
    }

    /**
     * Удаление информации о вкладе по ID
     */
    @DeleteMapping(value = "/persons/{id}")
    public ResponseEntity<?> deleteDepositById(@PathVariable(name = "id") int id) {
        final boolean deleted = dao.deleteDepositById(id);

        return deleted
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
}
