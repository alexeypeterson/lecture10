package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.JDBC.Deposit;
import ru.edu.dao.DepositDao;

import java.util.List;

@RestController
public class DepositController {
    private final DepositDao dao;

    @Autowired
    public DepositController(DepositDao dao){
        this.dao = dao;
    }

    /**
     * Создание нового вклада
     */
    @PostMapping(value = "/deposit")
    public ResponseEntity<?> createDeposit(@RequestBody Deposit deposit){
        dao.createDeposit(deposit);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Получение списка всех вкладов
     */
    @GetMapping(value = "/deposit")
    public ResponseEntity<List<Deposit>> readAllDeposits(){
        final List<Deposit> depositList = dao.readAllDeposits();

        return depositList != null &&  !depositList.isEmpty()
                ? new ResponseEntity<>(depositList, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
