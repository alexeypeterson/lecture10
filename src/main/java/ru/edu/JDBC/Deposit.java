package ru.edu.JDBC;

import java.util.Date;

public class Deposit {
    private int persID;
    private int startSum;
    private Date startDate;
    private double percent;

    public Deposit() {

    }

    public Deposit(int persID, int startSum, Date startDate, double percent) {
        this.persID = persID;
        this.startSum = startSum;
        this.startDate = startDate;
        this.percent = percent;
    }

    public int getPersID() {
        return persID;
    }

    public int getStartSum() {
        return startSum;
    }

    public Date getStartDate() {
        return startDate;
    }

    public double getPercent() {
        return percent;
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "persID=" + persID +
                ", startSum=" + startSum +
                ", startDate=" + startDate +
                ", percent=" + percent +
                '}';
    }
}
