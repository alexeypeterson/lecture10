package ru.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lecture10Application {

    public static void main(String[] args) {
        SpringApplication.run(Lecture10Application.class, args);
    }
}
